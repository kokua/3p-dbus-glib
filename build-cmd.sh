#!/bin/bash

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

PROJECT="dbus-glib"
VERSION="0.76"
SOURCE_DIR="$PROJECT-$VERSION"

DBUS_VERSION="1.2.1"
DBUS_SOURCE_DIR="dbus-$DBUS_VERSION"

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

stage="$(pwd)/stage"
case "$AUTOBUILD_PLATFORM" in
    "windows")
        echo "dbus-glib headers only for linux"
    ;;
    "darwin")
        echo "dbus-glib headers only for linux"
    ;;
    "linux")
        pushd "$SOURCE_DIR"
            # copy just the headers to the right place
            mkdir -p "$stage/include/dbus"
            cp -dp dbus/*.h "$stage/include/dbus"
        popd
        pushd "$DBUS_SOURCE_DIR"
            cp -dp dbus/*.h "$stage/include/dbus"
        popd
    ;;
esac
mkdir -p "$stage/LICENSES"
tail -n 31 "$SOURCE_DIR/COPYING" > "$stage/LICENSES/$PROJECT.txt"
cp -v "${SOURCE_DIR}/doc/reference/version.xml" "${stage}/VERSION.txt"

pass

